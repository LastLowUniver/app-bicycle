import {env} from "../env";

export class BicycleService {
    async getAllCars() {
        try {
            const res = await fetch(env.app_url + '/cars/active')
            return await res.json()
        } catch (e) {
            return {"message": "error"}
        }
    }

    async getOrders(value){
        try {
            const res = await fetch(`${env.app_url}/orders/${value}`)
            return await res.json()
        } catch (e) {
            return {"message": "error"}
        }
    }

    async createOrder(time, id, value) {
        await fetch(`${env.app_url}/orders/create/${time}/${id}/${value}`)
    }
}