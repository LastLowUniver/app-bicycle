import {env} from "../env";
import {ToastAndroid} from "react-native";

export class AuthService {
    async login(payload) {
        return this.request('/login', payload);
    }

    async register(payload) {
        return this.request('/register', payload);
    }

    async request(url, payload) {
        const res = await fetch(env.app_url + url, {
            method: 'POST',
            body: JSON.stringify(payload),
            headers: {
                'Content-Type': 'application/json'
            }
        })

        const data = await res.json();

        if (data.status !== "success") {
            ToastAndroid.showWithGravity(data.message, ToastAndroid.SHORT, ToastAndroid.TOP)
            return null;
        }

        return data;
    }
}