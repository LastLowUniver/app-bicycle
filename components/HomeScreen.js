import {useEffect, useState} from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {Image, StyleSheet, Text, TextInput, TouchableOpacity, View} from "react-native";
import {BicycleService} from "../services/bicycle.service";

export function HomeScreen({ navigation, route }) {

    const [item, setItem] = useState({})

    const [time, setTime] = useState('')

    const [disable, setDisable] = useState(true)

    useEffect(() => {
        setItem(route.params.item)
        navigation.setOptions({
            title: route.params.title
        })
    }, [])

    const changeTime = (elem) => {
        setDisable(!elem.length)
        setTime(elem)
    }

    const createOrder = async () => {
        await (new BicycleService()).createOrder(time, item.id, await AsyncStorage.getItem('@login'))

        navigation.replace('Заказы')
    }

    const styles = StyleSheet.create({
        input: {
            width: '100%',
            marginBottom: 30,
            paddingHorizontal: 20,
            paddingVertical: 10,
            borderBottomWidth: 1,
            borderRadius: 10,
            backgroundColor: "#cecece2a",
            fontSize: 16,
        }
    });

    return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'flex-start'}}>
            <Image
                style={
                    {
                        width: '80%',
                        height: 220,
                        borderRadius: 20,
                        marginTop: 20
                    }
                }
                source={{uri: item.img}}
            />
            <Text style={{
                fontSize: 24,
                marginTop: 30
            }}>{item.name}</Text>

            <Text style={{
                fontSize: 20,
                marginTop: 20
            }}>
                Цена за час: {item.amount_of_time} руб
            </Text>

            <Text style={{
                fontSize: 16,
                marginTop: 30,
                marginHorizontal: 20
            }}>{item.about}</Text>

            <View style={
                {
                    position: 'absolute',
                    justifyContent: 'flex-end',
                    bottom: 0,
                    left: 0,
                    backgroundColor: '#FFF',
                    width: '100%',
                }
            }>

                <Text style={{
                    fontSize: 16,
                    marginBottom: 10,
                    marginHorizontal: 20,
                    textAlign: 'center'
                }}> {disable ? '' : 'Стоимость: ' + time * item.amount_of_time}</Text>

                <TextInput
                    style={styles.input}
                    placeholder="Количество часов"
                    value={time}
                    onChangeText={changeTime}
                    keyboardType='numeric'
                />

                <TouchableOpacity
                    onPress={createOrder}
                    disabled={disable}
                    style={
                        !disable ?
                            {
                                backgroundColor: '#FF00FF',
                                width: '100%',
                                paddingVertical: 20
                            }
                            :
                            {
                                backgroundColor: '#cecece',
                                width: '100%',
                                paddingVertical: 20
                            }
                    }
                >
                    <Text
                        style={
                            {
                                textAlign: 'center',
                                color: '#FFF',
                                fontWeight: '600',
                                fontSize: 20
                            }
                        }
                    >
                        Заказать
                    </Text>
                </TouchableOpacity>
            </View>
        </View>
    );
}
