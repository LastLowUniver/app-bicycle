import {useContext, useState} from "react";
import {StyleSheet, Text, TextInput, ToastAndroid, TouchableOpacity, View} from "react-native";

export function SignUpScreen({ navigation, authContext }) {
    const [login, setLogin] = useState('lastlow987988');
    const [password, setPassword] = useState('098098098');

    const { login: loginAuth } = useContext(authContext);

    const styles = StyleSheet.create({
        input: {
            width: '95%',
            marginBottom: 30,
            paddingHorizontal: 20,
            paddingVertical: 10,
            borderBottomWidth: 1,
            borderRadius: 10,
            backgroundColor: "#cecece2a",
            fontSize: 16
        }
    });

    const loginFunc = async (payload) => {
        await loginAuth(payload);
    }

    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <TextInput
                style={styles.input}
                placeholder="Login"
                value={login}
                onChangeText={setLogin}
            />
            <TextInput
                style={styles.input}
                placeholder="Пароль"
                value={password}
                onChangeText={setPassword}
                secureTextEntry
            />
            <TouchableOpacity onPress={() => navigation.replace('Регистрация')}>
                <Text>Нет аккаунта</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={
                    {
                        position: 'absolute',
                        bottom: 0,
                        left: 0,
                        backgroundColor: '#FF00FF',
                        width: '100%',
                        paddingVertical: 20
                    }
                }
                onPress={() => loginFunc({ login, password })}
            >
                <Text
                    style={
                        {
                            textAlign: 'center',
                            color: '#FFF',
                            fontWeight: '600',
                            fontSize: 20
                        }
                    }
                >
                    Войти
                </Text>
            </TouchableOpacity>
        </View>
    );
}