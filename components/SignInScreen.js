import {useContext, useState} from "react";
import {StyleSheet, Text, TextInput, TouchableOpacity, View} from "react-native";

export function SignInScreen({ navigation, authContext }) {
    const [email, setEmail] = useState('')
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');

    const { register } = useContext(authContext);

    const loginFunc = async (payload) => {
        await register(payload)
    }

    const styles = StyleSheet.create({
        input: {
            width: '95%',
            marginBottom: 30,
            paddingHorizontal: 20,
            paddingVertical: 10,
            borderBottomWidth: 1,
            borderRadius: 10,
            backgroundColor: "#cecece2a",
            fontSize: 16
        }
    });


    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <TextInput
                style={styles.input}
                placeholder="Email"
                value={email}
                onChangeText={setEmail}
            />
            <TextInput
                style={styles.input}
                placeholder="Login"
                value={login}
                onChangeText={setLogin}
            />
            <TextInput
                style={styles.input}
                placeholder="Пароль"
                value={password}
                onChangeText={setPassword}
                secureTextEntry
            />
            <TouchableOpacity onPress={() => navigation.replace('Авторизация')}>
                <Text>Есть аккаунт</Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={
                    {
                        position: 'absolute',
                        bottom: 0,
                        left: 0,
                        backgroundColor: '#FF00FF',
                        width: '100%',
                        paddingVertical: 20
                    }
                }
                onPress={() => loginFunc({ email, login, password })}
            >
                <Text
                    style={
                        {
                            textAlign: 'center',
                            color: '#FFF',
                            fontWeight: '600',
                            fontSize: 20
                        }
                    }
                >
                    Войти
                </Text>
            </TouchableOpacity>
        </View>
    );
}