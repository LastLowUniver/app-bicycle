import {useEffect, useState} from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {FlatList, Text, View} from "react-native";
import {BicycleService} from "../services/bicycle.service";

export function OrderScreen({ navigation }) {
    const [data, setData] = useState([])

    useEffect(() => {
        const getData = async () => {
            setData(await (new BicycleService()).getOrders(await AsyncStorage.getItem('@login')))
        }

        getData()
    }, [])

    return (
        <View style={{
            flex: 1,
            alignItems: 'center',
            justifyContent: 'flex-start'
        }}>
            <FlatList
                style={{
                    width: '100%'
                }}
                data={data}
                renderItem={({ item }) =>
                    <View style={{
                        backgroundColor: "#FF00FF2a",
                        marginTop: 20,
                        marginHorizontal: 20,
                        padding: 20,
                        borderRadius: 20,
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        height: 120
                    }}>
                        <View style={{
                            justifyContent: 'space-between'
                        }}>
                            <Text style={{
                                fontSize: 25
                            }}>{item.carName}</Text>
                            <Text style={{
                                fontSize: 16
                            }}>Дата: {item.date_at}</Text>
                        </View>
                        <View style={{
                            justifyContent: 'space-between'
                        }}>
                            <Text style={{
                                textAlign: 'right',
                                fontSize: 20
                            }}>Цена: {item.amount}</Text>
                            <Text style={{
                                textAlign: 'right',
                                fontSize: 16
                            }}>Количество часов: {item.time}</Text>
                        </View>
                    </View>
                }
                keyExtractor={item => item.id.toString()}
            />
        </View>
    )
}