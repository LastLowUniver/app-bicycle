import {useContext, useEffect, useState} from "react";
import AsyncStorage from "@react-native-async-storage/async-storage";
import {StyleSheet, Text, TouchableOpacity, View} from "react-native";

export function MenuScreen({ navigation, authContext }) {

    const [login, setLogin] = useState('')

    const { logout } = useContext(authContext);

    const getLogin = async () => {
        const value = await AsyncStorage.getItem('@login')
        setLogin(value)
    }

    useEffect(() => {
        getLogin()
    }, [])

    const styles = StyleSheet.create({
        elem: {
            paddingVertical: 10,
            marginBottom: 1,
            width: '100%',
            backgroundColor: '#FF00FF'
        },
        text: {
            textAlign: 'center',
            color: '#FFF',
            fontSize: 16
        }
    })

    return (
        <View style={{
            flex: 1,
            alignItems: 'center',
            paddingTop: 20
        }}>
            <Text style={{
                marginVertical: 20,
                fontSize: 24
            }}>{login}</Text>
            <TouchableOpacity style={styles.elem} onPress={() => navigation.push('Главная')}>
                <Text style={styles.text}>Главная</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.elem} onPress={() => navigation.push('Заказы')}>
                <Text style={styles.text}>Заказы</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.elem} onPress={logout}>
                <Text style={styles.text}>Выйти</Text>
            </TouchableOpacity>
        </View>
    )
}
