import {useEffect, useState} from "react";
import {FlatList, Image, StyleSheet, Text, TouchableOpacity, View} from "react-native";
import {env} from "../env";
import {BicycleService} from "../services/bicycle.service";

export function LogScreen({ navigation }) {

    const [data, setData] = useState([])

    useEffect(() => {
        async function getFunc() {
            setData(await (new BicycleService()).getAllCars())
        }

        getFunc()
    }, []);

    const styles = StyleSheet.create({
        elem: {
            flexDirection: 'row',
            backgroundColor: "#FF00FF2a",
            marginHorizontal: 20,
            marginTop: 20,
            borderRadius: 20
        }
    })


    return (
        <View style={{ flex: 1, width: '100%', alignItems: 'center', justifyContent: 'center' }}>
            <FlatList
                style={{
                    width: '100%'
                }}
                data={data}
                renderItem={({ item }) =>
                    <TouchableOpacity style={styles.elem}
                                      onPress={() => navigation.push('home', {
                                          title: item.name,
                                          item
                                      })}
                    >
                        <Image
                            style={
                                {
                                    width: 170,
                                    height: 150,
                                    borderTopLeftRadius: 20,
                                    borderBottomLeftRadius: 20
                                }
                            }
                            source={{ uri: item.img }}
                        />
                        <View style={{ padding: 20 }}>
                            <Text style={{ fontSize: 20 }}>{item.name}</Text>
                            <Text style={{ marginTop: 30, fontSize: 16 }}>Цена за час: {item.amount_of_time} руб</Text>
                        </View>
                    </TouchableOpacity>
                }
                keyExtractor={item => item.id.toString()}
            />
            <TouchableOpacity style={{
                position: 'absolute',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 100,
                bottom: 30,
                right: 30,
                width: 70,
                height: 70,
                backgroundColor: "#FF00FF2a"
            }}
                              onPress={() => navigation.push('Меню')}
            >
                <Text>Меню</Text>
            </TouchableOpacity>
        </View>
    );
}