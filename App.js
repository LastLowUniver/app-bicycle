import {StyleSheet, Text, ToastAndroid} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {createContext, Suspense, useEffect, useMemo, useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {LogScreen} from "./components/LogScreen";
import {SignInScreen} from "./components/SignInScreen";
import {MenuScreen} from "./components/MenuScreen";
import {OrderScreen} from "./components/OrderScreen";
import {SignUpScreen} from "./components/SignUpScreen";
import {HomeScreen} from "./components/HomeScreen";
import {AuthService} from "./services/auth.service";


const AuthContext = createContext();
const Stack = createNativeStackNavigator();

function App() {
    const [userLogin, setUserLogin] = useState(null);

    async function checkLogin() {
        const value = await AsyncStorage.getItem('@login')
        if (value) setUserLogin(value)
    }

    useEffect(() => {
        checkLogin()
    }, [])

    const authContext = useMemo(
        () => ({
            login: async (payload) => {
                const data = await (new AuthService()).login(payload);
                if (data === null) return;

                await AsyncStorage.setItem('@login', payload.login)
                setUserLogin(payload.login)
            },
            register: async (payload) => {
                const data = await (new AuthService()).register(payload);
                if (data === null) return;

                await AsyncStorage.setItem('@login', payload.login)
                setUserLogin(payload.login)
            },
            logout: async () => {
                await AsyncStorage.clear()
                setUserLogin(null)
            }
        })
    )


    return (
        <Suspense fallback={<Text>Loading...</Text>}>
            <AuthContext.Provider value={authContext}>
                <NavigationContainer>
                    <Stack.Navigator initialRouteName="Главная">
                        {
                            userLogin == null ? (
                                <>
                                    <Stack.Screen
                                        name="Авторизация"
                                        children={(props) => <SignUpScreen {...props} authContext={AuthContext}/>}
                                    />
                                    <Stack.Screen
                                        name="Регистрация"
                                        children={(props) => <SignInScreen {...props} authContext={AuthContext}/>}
                                    />
                                </>
                            ) : (
                                <>
                                    <Stack.Screen name="Главная" component={LogScreen} />
                                    <Stack.Screen name="home" component={HomeScreen} />
                                    <Stack.Screen name="Заказы" component={OrderScreen} />
                                    <Stack.Screen
                                        name="Меню"
                                        children={(props) => <MenuScreen {...props} authContext={AuthContext}/>}
                                    />
                                </>
                            )
                        }
                    </Stack.Navigator>
                </NavigationContainer>
            </AuthContext.Provider>
        </Suspense>
    );
}

export default App;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
